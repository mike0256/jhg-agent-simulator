LINE_SEPARATOR = "============================"
BEGIN_MESSAGE = "\n{line_sep} BEGIN: {func_name} {line_sep}\n"
END_MESSAGE = "\n{line_sep} END: {func_name} {line_sep}\n"

def print_bars(func):
    def wrapper(*args, **kwargs):
        print_params = {
                    "func_name": func.__name__,
                    "line_sep": LINE_SEPARATOR
                    }

        print(BEGIN_MESSAGE.format(**print_params))
        
        result = func(*args, **kwargs)
        
        print(END_MESSAGE.format(**print_params))
        
        return result
    return wrapper

def print_args(func):
    def wrapper(*args, **kwargs):
        print_params = {
                    "func_name": func.__name__,
                    "line_sep": LINE_SEPARATOR
                    }

        print(BEGIN_MESSAGE.format(**print_params))
        
        args_message = "Calling function {}\n\targs: {}\n\tkwargs: {}"
        print(args_message.format(func.__name__, args, kwargs))
        result = func(*args, **kwargs)
        
        print(END_MESSAGE.format(**print_params))
        
        return result
    return wrapper