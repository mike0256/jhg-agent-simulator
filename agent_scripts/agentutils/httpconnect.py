import requests
import json

class HTTPGetError(Exception):
    pass

class HTTPPostError(Exception):
    pass

class HTTPConnection:

    def __init__(self, url):
        self.base_url = url

    def post_request(self, resource_url, post_params):
        URL = self.base_url + resource_url
        headers = {
            "Content-Type": "application/json"
        }

        response = requests.post(
                                url=URL, 
                                data=json.dumps(post_params), 
                                headers=headers)

        if 200 <= response.status_code <= 299:
            if response.text is not None and response.text != '':
                try:
                    response_data = response.json()
                    return response_data
                except ValueError:
                    return response.text
            else:
                return response.text
        else:
            error_msg = ("POST request failed with status {status_code} "
                            "for resource {resource} with params {params}")
            error_params = {
                        "status_code": response.status_code, 
                        "resource": resource_url, 
                        "params": post_params
                        }
            raise HTTPPostError(error_msg.format(**error_params))

    def get_request(self, resource_url, get_params):
        if resource_url[-1] != '/':
            resource_url += '/'

        # NOTE: This is barebones and doesn't handle html enities
        arg_list = ['{}={}'.format(k, v) for k, v in get_params.items()]
        URL = self.base_url + resource_url + "?" + '&'.join(arg_list)
        headers = {
            "Content-Type": "application/json"
        }

        response = requests.get(url=URL, headers=headers)

        if 200 <= response.status_code <= 299:
            response_data = response.json()
            return response_data
        else:
            error_msg = ("GET request failed with status {status_code} "
                            "for resource {resource} with params {params}")
            error_params = {
                        "status_code": response.status_code, 
                        "resource": resource_url,
                        "params": get_params
                        }
            raise HTTPGetError(error_msg.format(**error_params))
