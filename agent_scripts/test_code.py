# local import files
from serversimulator import GameServerProxy
from agentsimulator import AgentSimulator
from agentutils import agentdecorators
from agentutils import jhgutils
from simutils.visualutils import PlotSet

from basicagent import basic_agent_factory
from cs670agent import cs670_agent_factory
from assassin import assassin_agent_factory
from matcher import matcher_agent_factory
from bestie import bestie_agent_factory

# import packages
import argparse
import numpy as np
import random
import matplotlib.pyplot as plt

class RandomAgent(AgentSimulator):
    def __init__(self, code, base_url='http://www.juniorhighgame.com', 
            agent_name='ai', is_player=True, is_bot=True, bot_role=None, 
            bot_reg_key=None):
        super().__init__(code, base_url, agent_name, is_player, 
            is_bot, bot_role, bot_reg_key)

    def play_round(self):
        bestie = random.choice(self.player_list)

        round_transaction = [
            {
                "receiverName": player, 
                "amount": self.round_tokens if player == bestie else 0
            } for player in self.player_list
        ]
        self.submit_tokens(round_transaction)

    def new_message(self, message):
        pass

MIN_VAL = 0.0
MAX_VAL = 500.0

def time_float_type(arg):
    try:
        f = float(arg)
    except ValueError:    
        raise argparse.ArgumentTypeError("Must be a floating point number")
    if f < MIN_VAL or f > MAX_VAL:
        raise argparse.ArgumentTypeError(f"Argument must be in range [{MIN_VAL}, {MAX_VAL}]")
    return f

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--alpha", type=float, default=0.2, 
        help="the alpha value")
    parser.add_argument("-B", "--beta", type=float, default=0.75, 
        help="the beta value")
    parser.add_argument("-g", "--give", type=float, default=1.4, 
        help="the give coeficient")
    parser.add_argument("-k", "--keep", type=float, default=1.0, 
        help="the keep coeficient")
    parser.add_argument("-s", "--steal", type=float, default=1.1, 
        help="the steal coeficient")
    parser.add_argument("-p", "--popularity", type=float, default=100.0, 
        help="starting popularity for all players")
    parser.add_argument("-r", "--rounds", type=int, default=2, 
        help="number of rounds in the game")
    parser.add_argument("-t", "--round_time", type=time_float_type, default=30,
        help="seconds in each round")
    parser.add_argument("-c", "--chat", type=str,
        choices=[
            jhgutils.chat_type['default'], 
            jhgutils.chat_type['free'], 
            jhgutils.chat_type['anonymous'],
            jhgutils.chat_type['private']],
        default=jhgutils.chat_type['default'], 
        help="the chat type for the game")
    parser.add_argument("-v", "--visualize", type=str,
        choices=["none", "last", "all", "dash"],
        default="none", 
        help="which is any graphs should be shown after the game")

    args = parser.parse_args()

    server_proxy = GameServerProxy()
    server_proxy.create_game(alpha=args.alpha, beta=args.beta, give=args.give, 
        keep=args.keep, steal=args.steal, base_popularity=args.popularity, 
        chat_type=args.chat, round_time=args.round_time, round_num=args.rounds)

    game_code = server_proxy.game_code

    # Spawn and setup the agents
    BasicAgentSim = basic_agent_factory(is_simulated=True)
    num_basic_agents = 1
    for i in range(num_basic_agents):
        bot = BasicAgentSim(game_code, base_url=None, agent_name=f'basic #{i}', 
            is_player=True, is_bot=False)
        server_proxy.register(bot)

    MatcherAgentSim = matcher_agent_factory(is_simulated=True)
    num_matcher_agents = 5
    for i in range(num_matcher_agents):
        bot = MatcherAgentSim(game_code, base_url=None, agent_name=f'matcher #{i}', 
            is_player=True, is_bot=False)
        server_proxy.register(bot)

    BestieAgentSim = bestie_agent_factory(is_simulated=True)
    num_bestie_agents = 4
    for i in range(num_bestie_agents):
        bot = BestieAgentSim(game_code, base_url=None, agent_name=f'bestie #{i}', 
            is_player=True, is_bot=False)
        server_proxy.register(bot)

    AssassinAgentSim = assassin_agent_factory(is_simulated=True)
    num_assassin_agents = 2
    for i in range(num_assassin_agents):
        bot = AssassinAgentSim(game_code, base_url=None, agent_name=f'assassin #{i}', 
            is_player=True, is_bot=False)
        server_proxy.register(bot)

    #TODO: replace this with your agent class, leave other parameters the same
    #TODO: change the agent name to something cool
    CS670AgentSim = cs670_agent_factory(is_simulated=True)
    num_cs670_agents = 1
    for i in range(num_cs670_agents):
        bot = CS670AgentSim(game_code, base_url=None, agent_name=f'agent 670 #{i}', 
            is_player=True, is_bot=False)
        server_proxy.register(bot)

    server_proxy.start_game()

    server_proxy.wait()


    if args.visualize == "last":
        server_proxy.show_popularity_graph(server_proxy.curr_round - 1)
        server_proxy.show_tornado_graph(server_proxy.curr_round - 1)
        server_proxy.show_network_graph(server_proxy.curr_round - 1)
        server_proxy.show_transaction_graph(server_proxy.curr_round - 1)
    elif args.visualize == "all":
        for curr_round in range(server_proxy.curr_round):
            server_proxy.show_popularity_graph(curr_round)
            server_proxy.show_tornado_graph(curr_round)
            server_proxy.show_network_graph(curr_round)
            server_proxy.show_transaction_graph(curr_round)
    elif args.visualize == "dash":
        n_cols, n_rows = 3, 2
        plt.ion()
        multi_plot = PlotSet(n_cols, n_rows)
        for curr_round in range(server_proxy.curr_round):
            pop_axis = 1
            multi_plot.clear_axis(pop_axis)
            server_proxy.show_popularity_graph(curr_round, multi_plot.get_axis(pop_axis))

            torn_axis = 2
            multi_plot.clear_axis(torn_axis)
            server_proxy.show_tornado_graph(curr_round, multi_plot.get_axis(torn_axis))

            net_axis = 3
            multi_plot.clear_axis(net_axis)
            server_proxy.show_network_graph(curr_round, multi_plot.get_axis(net_axis))

            action_axis = 4
            multi_plot.clear_axis(action_axis)
            server_proxy.show_transaction_graph(curr_round, multi_plot.get_axis(action_axis))

            param_axis = 0
            multi_plot.clear_axis(param_axis)
            server_proxy.show_param_graph(curr_round, multi_plot.get_axis(param_axis))

            result_axis = 5
            multi_plot.clear_axis(result_axis)
            server_proxy.show_result_graph(curr_round, multi_plot.get_axis(result_axis))

            round_messages = server_proxy.get_round_messages(curr_round)
            if round_messages is not None:
                for message in round_messages:
                    print(message)

            multi_plot.draw()
            input(f"Press Enter to continue to round {curr_round + 1}...")
        input("Game Over!\nPress Enter to quit")