import matplotlib.pyplot as plt
import matplotlib

class GraphAxisError(Exception):
    pass

class PlotSet:
    def __init__(self, ncols, nrows):
        self.fig, self.axes = plt.subplots(ncols=ncols, nrows=nrows)

        self.fig.tight_layout(pad=3.0)
        self.colorbars = [None] * (ncols*nrows)

        font = {'weight': 'bold', 'size': 6}

        matplotlib.rc('font', **font)

    def clear_axis(self, plot_num):
        if plot_num >= len(self.axes.ravel()):
            raise GraphAxisError("Unknown plot number {plot_num}")
        im = self.axes.ravel()[plot_num].images
        if len(im) > 0:
            cb = im[-1].colorbar
            cb.remove()
        self.axes.ravel()[plot_num].clear()

    def get_axis(self, plot_num):
        if plot_num >= len(self.axes.ravel()):
            raise GraphAxisError("Unknown plot number {plot_num}")
        return self.axes.ravel()[plot_num]

    def draw(self):
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()
    
    def clear(self):
        self.fig.clf()