import numpy as np
from copy import deepcopy

class NetNode:
    def __init__(self, init_pos, name, code, popularity):
        self.position = []
        self.position.append(np.array(init_pos))
        self.code = code
        self.popularity = popularity
        self.min_margin = 1.25
        self.name = name
        self.init_step = 0.1
        self.step = self.init_step

    def get_size(self):
        return self.popularity

    def get_current_pos(self):
        return self.position[-1]

    def _generate_ideal_matrix(self, relation_mat):
        ideal_mat = deepcopy(relation_mat)
        ideal_mat[ideal_mat == 1.0] = 0.0
        np.fill_diagonal(ideal_mat, 0.0)
        
        sign_mat = np.sign(ideal_mat)
        
        for i in range(0, len(sign_mat)):
            for j in range(i, len(sign_mat)):
                if sign_mat[i, j] == -1 or sign_mat[j, i] == -1:
                    sign_mat[i, j] = sign_mat[j, i] = -1
                elif sign_mat[i, j] == 1 or  sign_mat[j, i] == 1:
                    sign_mat[i, j] = sign_mat[j, i] = 1
                else:
                    sign_mat[i, j] = sign_mat[j, i] = 0

        ideal_mat = np.abs(ideal_mat)
        ideal_mat = (ideal_mat + ideal_mat.T) / 2.0
        ideal_mat = ideal_mat * sign_mat

        ideal_max = np.amax(ideal_mat)
        ideal_min = np.amin(ideal_mat)
        ideal_mat[ideal_mat != 0] += ideal_max - ideal_min

        ideal_max = np.amax(ideal_mat)
        ideal_mat[ideal_mat != 0] = ideal_max / ideal_mat[ideal_mat != 0]
        ideal_mat[ideal_mat != 0] = np.round(ideal_mat[ideal_mat != 0], 
                                                decimals=2)

        np.fill_diagonal(ideal_mat, 0.0)
        
        return ideal_mat

    def update(self, others, relation_mat):
        dist = lambda x, y: ( (x[0] - y[0])**2 + (x[1] - y[1])**2 )**(1/2)

        self_pos = self.get_current_pos()

        pos_dot = np.zeros_like(self_pos)

        ideal_mat = self._generate_ideal_matrix(relation_mat)

        for other in others:
            other_pos = other.get_current_pos()
            if self.code != other.code \
                    and (ideal_mat[self.code, other.code] != 0.0 \
                        or ideal_mat[other.code, self.code] != 0.0 \
                        or self.min_margin > dist(self_pos, other_pos)):
                
                ideal_dist = ideal_mat[self.code, other.code]
                
                if ideal_dist < 1.0:
                    ideal_dist = self.min_margin

                x1_error = -2*(ideal_dist - ((self_pos[0] - other_pos[0])**2 \
                    + (self_pos[1] - other_pos[1])**2)**0.5)*(1.0*self_pos[0] \
                    - 1.0*other_pos[0])*((self_pos[0] - other_pos[0])**2 \
                    + (self_pos[1] - other_pos[1])**2)**(-0.5)
                
                x2_error = -2*(ideal_dist - ((self_pos[0] - other_pos[0])**2 \
                    + (self_pos[1] - other_pos[1])**2)**0.5)*(1.0*self_pos[1] \
                    - 1.0*other_pos[1])*((self_pos[0] - other_pos[0])**2 \
                    + (self_pos[1] - other_pos[1])**2)**(-0.5)

                pos_dot += np.array([x1_error, x2_error]).astype(np.float)

        self.position.append(self.position[-1] - self.step * pos_dot)
