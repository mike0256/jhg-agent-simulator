# local import files
from serversimulator import GameServerProxy
from agentsimulator import AgentSimulator
from agentutils import agentdecorators
from agentutils import jhgutils

from basicagent import basic_agent_factory
from cs670agent import cs670_agent_factory
from assassin import assassin_agent_factory
from matcher import matcher_agent_factory
from bestie import bestie_agent_factory

# import packages
import argparse
import numpy as np
import random

class RandomAgent(AgentSimulator):
    def __init__(self, code, base_url='http://www.juniorhighgame.com', 
            agent_name='ai', is_player=True, is_bot=True, bot_role=None, 
            bot_reg_key=None):
        super().__init__(code, base_url, agent_name, is_player, 
            is_bot, bot_role, bot_reg_key)

    def play_round(self):
        bestie = random.choice(self.player_list)

        round_transaction = [
            {
                "receiverName": player, 
                "amount": self.round_tokens if player == bestie else 0
            } for player in self.player_list
        ]
        self.submit_tokens(round_transaction)

    def new_message(self, message):
        pass

MIN_VAL = 0.0
MAX_VAL = 500.0

def time_float_type(arg):
    try:
        f = float(arg)
    except ValueError:    
        raise argparse.ArgumentTypeError("Must be a floating point number")
    if f < MIN_VAL or f > MAX_VAL:
        raise argparse.ArgumentTypeError(f"Argument must be in range [{MIN_VAL}, {MAX_VAL}]")
    return f

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-c", "--code", type=str, required=True, 
        help="code for the game to join"
    )

    args = parser.parse_args()

    args = parser.parse_args()

    game_code = args.code
    #web_url = 'http://www.juniorhighgame.com'
    web_url = 'http://localhost:8080'
    is_bot = False

    # Spawn and setup the agents
    class_agents = [
        (basic_agent_factory, 'basic', 3),
        (matcher_agent_factory, 'matcher', 0),
        (bestie_agent_factory, 'bestie', 0),
        (assassin_agent_factory, 'assassin', 10),
        (matcher_agent_factory, 'matcher', 0)
    ]

    for agent_factory, agent_name, num_agents in class_agents:
        OnlineAgent = agent_factory(is_simulated=False)
        for i in range(num_agents):
            bot_name = f'{agent_name} #{i}'
            bot = OnlineAgent(game_code, base_url=web_url, agent_name=bot_name, 
                is_player=True, is_bot=is_bot)

    bot.wait()