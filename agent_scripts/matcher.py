# import local files
from agentconnection import AgentConnection
from agentsimulator import AgentSimulator
from agentutils import agentdecorators
from agentutils import jhgutils

# import packages
import argparse
import random
import numpy as np

def matcher_agent_factory(is_simulated=False):
    ConnectionClass = None
    if is_simulated:
        AgentType = AgentSimulator
    else:
        AgentType = AgentConnection

    class MatcherAgent(AgentType):
        def __init__(self, code, base_url='http://www.juniorhighgame.com', 
                agent_name='ai', is_player=True, is_bot=True, bot_role=None, 
                bot_reg_key=None):
            super().__init__(code, base_url, agent_name, is_player, is_bot, 
                bot_role, bot_reg_key)

        def play_round(self):
            if self.round_num == 1:
                bestie = random.choice(self.player_list)

                round_transaction = [
                    {
                        "receiverName": player, 
                        "amount": self.round_tokens if player == bestie else 0
                    } for player in self.player_list
                ]
            else:
                curr_pops = self.round_data[self.round_num]['playerPopularities']
                popularity_lookup = {}
                for pop_data in curr_pops:
                    popularity_lookup[pop_data['name']] = pop_data['popularity']

                exchanges = self.round_data[self.round_num]['exchanges']
                received_lookup = {}
                for received in exchanges['received']:
                    received_lookup[received['name']] = received['amount']
                
                alloc_vec = np.zeros(len(self.player_list))
                for player in self.player_list:
                    player_idx = self.player2idx[player]
                    player_pop = popularity_lookup[player]
                    player_tokens = received_lookup[player]
                    alloc_vec[player_idx] = player_pop * player_tokens

                if np.sum(np.abs(alloc_vec)) != 0:
                    # normalize the allocation vector using L1 norm
                    token_vec = np.sign(alloc_vec) \
                        * np.round(np.abs(alloc_vec) / np.sum(np.abs(alloc_vec)) \
                            * self.round_tokens)
                else:
                    token_vec = np.zeros(len(self.player_list))
                    bestie = random.choice(self.player_list)
                    token_vec[self.player2idx[bestie]] = self.round_tokens
                    tokens_used = np.sum(np.abs(token_vec))

                tokens_used = np.sum(np.abs(token_vec))

                while tokens_used != self.round_tokens:
                    if tokens_used > self.round_tokens:
                        # take tokens from the lowest
                        min_idx = np.argmax(np.abs(token_vec[np.nonzero(tokens_used)]))
                        min_sign = np.sign(token_vec)[min_idx]
                        indices = np.argwhere(token_vec == token_vec[min_idx]).flatten()
                        change_idx = random.choice(indices)
                        token_vec[change_idx] -= min_sign
                    elif tokens_used < self.round_tokens:
                        # add tokens to the highest
                        max_idx = np.argmax(np.abs(token_vec))
                        max_sign = np.sign(token_vec)[max_idx]
                        indices = np.argwhere(token_vec == token_vec[max_idx]).flatten()
                        change_idx = random.choice(indices)
                        token_vec[change_idx] += max_sign
                    tokens_used = np.sum(np.abs(token_vec))

                round_transaction = [
                    {
                        "receiverName": player, 
                        "amount": token_vec[self.player2idx[player]]
                    } for player in self.player_list
                ]

            self.submit_tokens(round_transaction)

    return MatcherAgent
  


      
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-c", "--code", type=str, required=True, 
        help="code for the game to join"
    )

    args = parser.parse_args()

    MatcherOnlineAgent = matcher_agent_factory(is_simulated=False)

    bot = MatcherOnlineAgent(args.code, base_url='http://www.juniorhighgame.com', 
        agent_name='matcher #i', is_player=True, is_bot=False)
        
    bot.wait()
    