# import local files
from agentconnection import AgentConnection
from agentsimulator import AgentSimulator
from agentutils import agentdecorators
from agentutils import jhgutils

# import packages
import argparse

def assassin_agent_factory(is_simulated=False):
    ConnectionClass = None
    if is_simulated:
        AgentType = AgentSimulator
    else:
        AgentType = AgentConnection

    class AssassinAgent(AgentType):
        def __init__(self, code, base_url='http://www.juniorhighgame.com', 
                agent_name='ai', is_player=True, is_bot=True, bot_role=None, 
                bot_reg_key=None):
            super().__init__(code, base_url, agent_name, is_player, is_bot, 
                bot_role, bot_reg_key)

        def play_round(self):
            if self.round_num == 1:
                round_transaction = [
                    {
                        "receiverName": player, 
                        "amount": self.round_tokens if player == self.player_name else 0
                    } for player in self.player_list
                ]
            else:
                curr_pops = self.round_data[self.round_num]['playerPopularities']
                lowest_pop = float('inf')
                weakest_player = None
                for player_data in curr_pops:
                    if player_data['name'] != self.player_name and 0.0 < player_data['popularity'] < lowest_pop:
                        lowest_pop = player_data['popularity']
                        weakest_player = player_data['name']

                if weakest_player is not None:
                        round_transaction = [
                            {
                                "receiverName": player,
                                "amount": -1 * self.round_tokens if player == weakest_player else 0
                            } for player in self.player_list
                        ]
                else:
                    round_transaction = [
                        {
                            "receiverName": player, 
                            "amount": self.round_tokens if player == self.player_name else 0
                        } for player in self.player_list
                    ]

            self.submit_tokens(round_transaction) 

    return AssassinAgent

        
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-c", "--code", type=str, required=True, 
        help="code for the game to join"
    )

    args = parser.parse_args()

    AssassinOnlineAgent = assassin_agent_factory(is_simulated=False)

    for i in range(6):
        bot = AssassinOnlineAgent(args.code, base_url='http://www.juniorhighgame.com', 
            agent_name='assassin #{:03d}'.format(i + 1), is_player=True,
            is_bot=False)
        
    bot.wait()
    